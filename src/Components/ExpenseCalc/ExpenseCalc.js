import React, {useState} from 'react';
import {nanoid} from "nanoid";
import './ExpenseCalc.css';

const Calculator = () => {
    const [mode, setMode] = useState('split');
    const [showResult, setShowResult] = useState('calculateNone');
    const [numberOfPeople, setNumberOfPeople] = useState('');
    const [sumOfOrder, setSumOfOrder] = useState('');
    const [percent, setPercent] = useState('');
    const [delivery, setDelivery] = useState('');
    const calculate = () => {
        setShowResult('calculate');
    };

    const [people, setPeople] = useState([]);
    const onRadioChange = e => {
        setMode(e.target.value);
    };
    const addPerson = () => {
        setPeople(people => [...people, {name: '', price: '', id: nanoid()}])
    };
    // const removePerson = id => {
    //     setPeople(people.filter(p => p.id !== id));
    // };
    const changePersonField = (id, name, value) => {
        setPeople(people => {
            return people.map(person => {
                if (person.id === id) {
                    return {...person, [name]: value}
                }
                return person;
            })
        })
    };

    let totalSum = null;

    for (let i = 0; i < people.length; i++) {
        totalSum = Number(totalSum) + Number(people[i].price);
    }
    return (
        <div>
            <h4>Сумма заказа считается: </h4>
            <div>
                <label>
                    <input type="radio"
                           checked={mode === 'split'}
                           name="mode"
                           value="split"
                           onChange={onRadioChange}
                    />{' '}
                    Поровну между всеми участниками
                </label>
            </div>
            <div>
                <label>
                    <input type="radio"
                           checked={mode === 'individual'}
                           name="mode"
                           value="individual"
                           onChange={onRadioChange}
                    />
                    Каждому индивидуально
                </label>
            </div>
            <div>
                {mode === 'split' ? (
                    <form>
                        <p>Человек: <input type="number" value={numberOfPeople}
                                           onChange={e => setNumberOfPeople(e.target.value)}/> чел.</p>
                        <p>Сумма заказа: <input type="number" value={sumOfOrder}
                                                onChange={e => setSumOfOrder(e.target.value)}/> сом.</p>
                        <p>Процент чаевых: <input type="number" value={percent}
                                                  onChange={e => setPercent(e.target.value)}/> %.</p>
                        <p>Доставка: <input type="number" value={delivery}
                                            onChange={e => setDelivery(e.target.value)}/> сом.</p>
                        <button type="button" onClick={calculate}>Рассчитать</button>

                        <div className={showResult}>
                            <p>Общая
                                сумма: {Number(sumOfOrder) + (Number(percent) * Number(sumOfOrder) / 100) + Number(delivery)} сом</p>
                            <p>Количество человек: {numberOfPeople}</p>
                            <p>Каждый платит
                                по: {Math.ceil((Number(sumOfOrder) + (Number(percent) * Number(sumOfOrder) / 100) + Number(delivery)) / Number(numberOfPeople))} сом</p>
                        </div>

                    </form>) : (
                    <form>
                        <h4>Individual form</h4>
                        {people.map(person => (
                            <div key={person.id}>
                                <input
                                    type="text"
                                    placeholder="Name"
                                    value={person.name}
                                    onChange={(e) => changePersonField(person.id, 'name', e.target.value)}
                                />
                                <input
                                    type="number"
                                    placeholder="Sum"
                                    value={person.price}
                                    onChange={(e) => changePersonField(person.id, 'price', e.target.value)}/> сом
                                {/*<button type="button" onClick={removePerson(person.id)}>Remove</button>*/}
                            </div>
                        ))}
                        <button type="button" onClick={addPerson}>Add person</button>

                        <p>Процент чаевых: <input type="number" value={percent}
                                                  onChange={e => setPercent(e.target.value)}/> %.</p>
                        <p>Доставка: <input type="number" value={delivery}
                                            onChange={e => setDelivery(e.target.value)}/> сом.</p>
                        <button type="button" onClick={calculate}>Рассчитать</button>
                        <div className={showResult}>
                            <p>Общая сумма: {totalSum} сом</p>
                            {people.map(person => (
                                <div key={person.id}>
                                    <p>{person.name}: {Math.ceil((Number(person.price)) + (Number(percent) * Number(person.price) / 100) + Number(delivery) / people.length)}</p>
                                </div>
                            ))}
                        </div>
                    </form>
                )}
            </div>
        </div>
    );
};

export default Calculator;
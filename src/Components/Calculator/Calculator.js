// import React, {useState} from 'react';
// import {nanoid} from "nanoid";
//
// const Calculator = () => {
//     const [mode, setMode] = useState('individual');
//     const [people, setPeople] = useState([]);
//     const onRadioChange = e => {
//         setMode(e.target.value);
//     };
//
//     const addPerson = () => {
//         setPeople(people => [...people, {name: '', price: '', id: nanoid()}])
//     };
//
//     const changePersonField = (id, name, value) => {
//       setPeople(people => {
//           return people.map(person => {
//               if (person.id === id) {
//                   return {...person, [name]: value}
//               }
//               return person;
//           })
//       })
//     };
//
//     return (
//         <div>
//             <div>
//                 <label>
//                     <input type="radio"
//                            checked={mode === 'split'}
//                            name="mode"
//                            value="split"
//                            onChange={onRadioChange}
//                     />{' '}
//                     Поровну между участниками
//                 </label>
//             </div>
//             <div>
//                 <label>
//                     <input type="radio"
//                            checked={mode === 'individual'}
//                            name="mode"
//                            value="individual"
//                            onChange={onRadioChange}
//                     />
//                     Каждому
//                     индивидуально
//                 </label>
//             </div>
//             <p>
//                 {mode === 'split' ? (
//                     <form>
//                         Split form
//                         <input type="text"/>
//                     </form>) : (
//                     <form>
//                         <h4>Individual form</h4>
//                         {people.map(person => (
//                             <div key={person.id}>
//                             <input
//                                 type="text"
//                                 placeholder="Name"
//                                 value={person.name}
//                                 onChange={(e) => changePersonField(person.id, 'name', e.target.value)}
//                             />
//                             <input type="number" placeholder="Sum"/>
//                                 <button type="button">Remove</button>
//                             </div>
//                         ))}
//                         <button type="button" onClick={addPerson}>Add person</button>
//                     </form>
//                 )}
//             </p>
//
//         </div>
//     );
// };
//
// export default Calculator;
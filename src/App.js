import './App.css';
import ExpenseCalc from "./Components/ExpenseCalc/ExpenseCalc";

function App() {
    return (
        <ExpenseCalc/>
    );
}

export default App;
